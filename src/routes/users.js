const express = require("express");
const router = express.Router();

const userData = require("../data/userData");

router.get("/", async (req, res) => {
  const dados = await userData();
  res.json(dados);
});

router.get("/websites", async (req, res) => {
  const dados = await userData();
  const websites = dados.map(dado => {
    const userWebsite = {
      id: dado.id,
      website: dado.website
    };

    return userWebsite;
  });

  res.json(websites);
});

router.get("/users", async (req, res) => {
  const dados = await userData();

  function ordena(users) {
    users.sort(function(a, b) {
      return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
    });
    return users;
  }

  const users = dados.map(dado => {
    const usersDetails = {
      id: dado.id,
      name: dado.name,
      email: dado.email,
      empresa: dado.company.name
    };

    return usersDetails;
  });

  const usersOrdem = ordena(users);

  res.json(usersOrdem);
});

router.get("/suites", async (req, res) => {
  const dados = await userData();
  const buscaSuite = suite => suite.address.suite.search(/suite/i) === 0
  const suites = dados.filter(buscaSuite)

  res.json(suites);
});

module.exports = router;
