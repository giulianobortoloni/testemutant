const fetch = require("node-fetch");

module.exports = function() {
  const dados = fetch("https://jsonplaceholder.typicode.com/users")
    .then(res => res.json())
    .then(json => {
      return json;
    });
  return dados;
};
