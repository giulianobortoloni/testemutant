const userData = require("./userData");

test("works with async/await userData ", async () => {
  const dados = await userData();
  expect(dados).not.toBeNull();
  expect(dados).not.toBeUndefined();
});